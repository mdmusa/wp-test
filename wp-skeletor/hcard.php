<?php
/**
* Template Name: HCard Preview
*/
get_header();?>


<section id="hcard">
    <div class="container">
        <div class="row title-head">
            <div class="col s12">
                <h1 class="title">hCard Builder</h1>
            </div>
        </div>
        <div class="row">
            <div class="col s6" id="frontend-frm">
                    <?php
                        gravity_form( 1, false, false, true, '', false );
                    ?>

            </div>
            <div class="col s6">
                <div class="preview-pan">
                    <?php
                        $arg = array(
                            'post_type'             => 'post',
                            'posts_per_page'        => 1,
                            'order'   => 'DESC',
                        );
                        $loop = new WP_Query($arg);
                        while($loop->have_posts()): $loop->the_post();?>

                            <h1 class="post-title">
                                <?php
                                   the_title();
                                ?>
                            </h1>
                            <div class="post-tags">
                                <?php
                                echo get_the_tag_list('<p>Tags: ',', ','</p>');
                                ?>
                            </div>
                            <div class="post-img">
                                <?php
                                the_post_thumbnail('preview_img');
                                ?>
                            </div>
                            <div class="post-content">
                                <?php
                                    the_content();
                                ?>
                            </div>

                            <div class="post-address">
                                <span><?php _e('Address ','text-domain');?></span><span class="address-txt"><?php echo $address = get_field("address");?></span>
                            </div>
                            <div class="post-gallery">
                                <p>
                                    <?php _e('Gallery ','text-domain')?>
                                </p>
                                <?php
                                $images = get_field('gallery');

                                if( $images ): ?>
                                    <ul>
                                        <?php foreach( $images as $image ):?>
                                            <li>
                                                <a href="<?php echo $image['url']; ?>">
                                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                </a>
                                            </li>

                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>


                            </div>
                            <div class="post-url">
                                <p>
                                    <?php _e('URL ','text-domain')?>
                                </p>
                                <?php
                                    $url_list = get_field('url_list');
                                    if ($url_list): ?>
                                        <ul>
                                            <?php
                                                while(has_sub_field('url_list')):
                                                    $url = get_sub_field('url');
                                                    ?>
                                                    <li>
                                                        <a target="_blank" href="<?php echo $url; ?>"><?php echo $url; ?></a>
                                                    </li><?php
                                                endwhile;
                                            ?>
                                        </ul>
                                    <?php endif;

                                ?>
                            </div>


                            <?php
                        endwhile;
                        wp_reset_query();
                        wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
get_footer();