<?php
/**
 * Theme Enqueue
 *
 * @package WordPress
 * @subpackage SKEL-ETOR
 * @since SKEL-ETOR 1.0
 */

/**
 * Theme styles
 *
 * @example wp_enqueue_style( $handle, $src , $deps, $ver, $in_footer );
 */

add_action('wp_enqueue_scripts', 'skel_etor_enqueue_styles', 20, 1);
function skel_etor_enqueue_styles() {
	// Styles loaded for front end only
	if ( ! is_admin() ) {
		wp_enqueue_style('style', get_template_directory_uri().'/style.css');
	}

}

//add_action('wp_enqueue_scripts', 'skel_etor_cdn_resources', 10, 1);		// @see enqueue.php
//add_action('wp_enqueue_scripts', 'skel_etor_enqueue_styles', 20, 1);	// @see enqueue.php
//add_action('wp_enqueue_scripts', 'skel_etor_enqueue_scripts', 30, 1);	// @see enqueue.php
/**
 * Theme scripts
 *
 * @example wp_enqueue_script( $handle => string, $src => string, $deps => array(), $ver => string, $in_footer => bool );
 */
//add_action('wp_enqueue_scripts', 'skel_etor_enqueue_scripts', 30, 1);
function skel_etor_enqueue_scripts() {
	if ( ! is_admin() ) {
		//wp_enqueue_script('modernizr', get_template_directory_uri().'/js/libs/modernizr-2.6.2.min.js');
		wp_enqueue_script('chromeframe', '//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js');
		wp_enqueue_script('respond', get_template_directory_uri().'/js/min/respond.min.js', array(), '1.0', true);
		wp_enqueue_script('plugins', get_template_directory_uri().'/js/min/plugins.min.js', array('jquery'), '1.0', true);
		wp_enqueue_script('script', get_template_directory_uri().'/js/min/script.min.js', array('jquery', 'plugins'), '1.0', true);
	}


}

/**
 * External resoruces
 */

/**
 * CDN Resources
 */

//add_action('wp_enqueue_scripts', 'skel_etor_cdn_resources', 10, 1);
function skel_etor_cdn_resources() {

	if ( ! is_admin() ) {

		/**
		 * Flexslider
		 *
		 * @link http://cdnjs.com/libraries/flexslider
		 */
		if ( SKEL_ETOR_FLEXSLIDER ) {
			wp_enqueue_style('flexslider', '//cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.0/flexslider-min.css');
			wp_enqueue_script('flexslider', '//cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.0/jquery.flexslider-min.js', array(), '1.0', true);
		}

		/**
		 * FontAwesome
		 *
		 * @link http://fontawesome.io/
		 */
		if ( SKEL_ETOR_FONTAWESOME ) {
			wp_enqueue_style('font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css');
		}

		/**
		 * Google Fonts
		 */
		if ( SKEL_ETOR_GOOGLEFONTS ) {
			wp_enqueue_style( 'skel-etor-google-font', '//fonts.googleapis.com/css?family=Karla', array(), '1.0' );
		}

	}
}

/**
 * Google Maps API
 *
 * Use conditional loading to limit API calls
 */
//add_action('wp_enqueue_scripts', 'skel_etor_google_maps_api');
function skel_etor_google_maps_api() {
	// Update condition
	if ( get_post_type() == 'gmaps_post_type_condition' ) {
		wp_enqueue_script('google-maps', '//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');
	}
}
if ( ! function_exists( 'wp_test_scripts' ) ){
    function wp_test_scripts() {
        // Add Material scripts and styles

        wp_enqueue_script( 'material-jquery', 'http://code.jquery.com/jquery-2.1.3.min.js', array(), '1.0', false );
        wp_enqueue_style('material-icons','https://fonts.googleapis.com/icon?family=Material+Icons','',false);
        wp_enqueue_style( 'material-style', get_template_directory_uri() . '/css/materialize.css' );
        wp_enqueue_style( 'material-main_style', get_template_directory_uri() . '/css/style.css' );
        wp_enqueue_script( 'material-script', get_template_directory_uri() . '/js/materialize.js', array(), '1.0', false );
        wp_enqueue_script( 'material-main', get_template_directory_uri() . '/js/main.js', array(), '', true );




    }
}
add_action( 'wp_enqueue_scripts', 'wp_test_scripts' );